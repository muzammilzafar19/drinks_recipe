package com.example.find_drink.notification_alert

import android.app.AlarmManager

import android.app.PendingIntent
import android.content.Context

import android.content.Intent
import java.util.*
import javax.inject.Inject


class AlarmManagerHelper @Inject constructor(val context: Context) {
    fun setAlarm() {
        val alarmIntent = Intent(context, NotificationReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            context,
            0,
            alarmIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val manager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        val date = Date()
        val calendar: Calendar = Calendar.getInstance()
        calendar.setTimeInMillis(System.currentTimeMillis())
        calendar.set(Calendar.HOUR_OF_DAY, 2)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        if (calendar.before(Calendar.getInstance())) {
            calendar.add(Calendar.DATE, 1)
        }
        manager?.setInexactRepeating(
            AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
            AlarmManager.INTERVAL_DAY, pendingIntent
        )
/*        val editor: SharedPreferences.Editor =
            getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit()
        editor.putInt("Currenttimehours", 2)
        editor.putInt("Currenttimeminutes", 0)
        editor.apply()*/
    }
}