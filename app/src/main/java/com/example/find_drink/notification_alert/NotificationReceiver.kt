package com.example.find_drink.notification_alert

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.find_drink.notification_alert.notification.NotificationManager
import com.example.find_drink.room.dao.FarvouriteDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


class NotificationReceiver @Inject constructor(
    val farvouriteDao: FarvouriteDao,
    val notificationManager: NotificationManager
) : BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                if (farvouriteDao.getSpecificFavourite().isNotEmpty()) {
                    val fav = farvouriteDao.getrandomFavourite()
                    notificationManager.createNotification(fav.drinkName, fav.thumnail, p0!!)
                }
            } catch (e: Exception) {
            }

        }
        // createNotification()

    }

    /*  fun createNotification(
          title: String?,
          body: String?,
          image_url: String?,
          context: Context,
          notificationsId: Int,
          single_id: String?
      ) {
          val `when` = System.currentTimeMillis()
          val id = System.currentTimeMillis().toInt()
          val bitmap = getBitmapFromURL(image_url)
          val notifystyle = NotificationCompat.BigPictureStyle()
          notifystyle.bigPicture(bitmap)
          val contentView = RemoteViews(context.packageName, R.layout.custom_layout)
          contentView.setImageViewBitmap(R.id.image, bitmap)
          contentView.setTextViewText(R.id.title, body)
          val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(context)
              .setSmallIcon(R.mipmap.ic_launcher)
              .setStyle(notifystyle)
              .setCustomBigContentView(contentView)
              .setContentText(body)
          val mNotificationManager: NotificationManager = context
              .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
          val notificationIntent: Intent = Intent(context, DashBoardActivity::class.java) //Sritapana189
          notificationIntent.putExtra("single_id", single_id)
          notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
          val contentIntent = PendingIntent.getActivity(context, id, notificationIntent, 0) //Modified
          val notification: Notification = mBuilder.build()
          notification.contentIntent = contentIntent
          notification.flags = notification.flags or Notification.FLAG_AUTO_CANCEL
          notification.defaults = notification.defaults or Notification.DEFAULT_SOUND
          notification.defaults = notification.defaults or Notification.DEFAULT_VIBRATE
          mNotificationManager.notify(notificationsId, notification)
      }

      fun getBitmapFromURL(strURL: String?): Bitmap? {
          return try {
              val url = URL(strURL)
              val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
              connection.setDoInput(true)
              connection.connect()
              val input: InputStream = connection.getInputStream()
              BitmapFactory.decodeStream(input)
          } catch (e: IOException) {
              e.printStackTrace()
              null
          }
      }*/


}