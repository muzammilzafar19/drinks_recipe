package com.example.find_drink.notification_alert.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.find_drink.R
import com.example.find_drink.activities.DashBoardActivity
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
//import com.momentolabs.app.securityCalculatorScreenActivity.ui.main.MainActivity
import javax.inject.Inject

class NotificationManager @Inject constructor(val context: Context) {

    fun createNotification(
        body: String?,
        image_url: String?,
        context: Context,
    ): Notification {
        createAppLockerServiceChannel()

        val id = System.currentTimeMillis().toInt()
        val bitmap = getBitmapFromURL(image_url)
        val notifystyle = NotificationCompat.BigPictureStyle()
        notifystyle.bigPicture(bitmap)
        val contentView = RemoteViews(context.packageName, R.layout.custom_layout)
        contentView.setImageViewBitmap(R.id.image, bitmap)
        contentView.setTextViewText(R.id.title, body)

        val resultIntent = Intent(context, DashBoardActivity::class.java)
        val resultPendingIntent =
            PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notification = NotificationCompat.Builder(context, CHANNEL_ID_DRINKS_REC)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setStyle(notifystyle)
            .setCustomBigContentView(contentView)
            .setContentText(body)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(resultPendingIntent)
            .build()

        NotificationManagerCompat.from(context)
            .notify(NOTIFICATION_ID_DRINKS_REC, notification)
        return notification
    }


    private fun createAppLockerServiceChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "name"
            val descriptionText = "description"
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel =
                NotificationChannel(CHANNEL_ID_DRINKS_REC, name, importance).apply {
                    description = descriptionText
                }
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun getBitmapFromURL(strURL: String?): Bitmap? {
        return try {
            val url = URL(strURL)
            val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input: InputStream = connection.getInputStream()
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }

    companion object {
        private const val CHANNEL_ID_DRINKS_REC = "CHANNEL_ID_DRINKS_REC"
        private const val NOTIFICATION_ID_DRINKS_REC = 1
        private const val NOTIFICATION_ID_APPLOCKER_PERMISSION_NEED = 2
    }
}