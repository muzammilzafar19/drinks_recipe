package com.example.find_drink.util

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class SharedPrefs @Inject constructor(val context: Context) {
    private lateinit var prefs: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    val IMAGE = "IMAGE"

    init {

        prefs = context.getSharedPreferences("connectPrefs", Context.MODE_PRIVATE)
        editor = prefs.edit()
        editor.apply()

    }

    fun settype(tyep: Int) {
        editor.putInt("type", tyep)
        editor.commit()
    }

    fun gettype(): Int {
        return prefs.getInt("type", 1)
    }

    fun setLastSearch(preioussearch: String?) {
        editor.putString("preioussearch", preioussearch)
        editor.commit()
    }

    fun getLastSearch(): String? {
        return prefs.getString("preioussearch", "margarita")
    }

    fun setAlarm(flag: Boolean) {
        editor.putBoolean("alarm", flag)
        editor.commit()
    }

    fun getAlarm() : Boolean {
        return prefs.getBoolean("alarm", false)

    }

}