package com.example.find_drink.di

import android.content.Context
import com.example.find_drink.network_module.ApiInterface
import com.example.find_drink.notification_alert.AlarmManagerHelper
import com.example.find_drink.notification_alert.NotificationReceiver
import com.example.find_drink.notification_alert.notification.NotificationManager
import com.example.find_drink.repository.MainRepo
import com.example.find_drink.room.dao.FarvouriteDao
import com.example.find_drink.util.ConnectionDetector
import com.example.find_drink.util.Constants
import com.example.find_drink.util.SharedPrefs
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    fun provideBaseUrl() = Constants.BASE_URL


    @Singleton
    @Provides
    fun hProvidesLogginInterceptor() = HttpLoggingInterceptor()

    @Singleton
    @Provides
    fun hProvidesGson(): Gson {
        return GsonBuilder()
            .setPrettyPrinting()
            .setLenient()
            .create()
    }





    @Singleton
    @Provides
    fun hProvidesHttpClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder().connectTimeout(4, TimeUnit.SECONDS)
            .addInterceptor(
                loggingInterceptor.setLevel(
                    HttpLoggingInterceptor.Level.BODY
                )
            )
            .build()
    }


    @Singleton
    @Provides
    fun hProvidesRetrofitService(
        httpClient: OkHttpClient,
        gson: Gson
    ): ApiInterface {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(
                GsonConverterFactory.create(
                    gson
                )
            )
            .client(httpClient)
            .build()
            .create(ApiInterface::class.java)

    }

    @Provides
    fun procidesNotificationManager(@ApplicationContext context: Context): NotificationManager {
        return NotificationManager(context)
    }


    @Provides
    fun provideConnectionDetector(@ApplicationContext context: Context): ConnectionDetector {
        return ConnectionDetector(context)
    }

    @Provides
    fun provideMainRepo(apiHelper: ApiInterface,farvouriteDao : FarvouriteDao,connectionDetector : ConnectionDetector): MainRepo {
        return MainRepo(apiHelper,farvouriteDao,connectionDetector)
    }

    @Provides
    fun provideNotificationReceiver(farvouriteDao : FarvouriteDao,NotificationManager : NotificationManager): NotificationReceiver {
        return NotificationReceiver(farvouriteDao,NotificationManager)
    }

    @Provides
    fun provideSharedPrefs(@ApplicationContext context: Context): SharedPrefs {
        return SharedPrefs(context)
    }
    @Provides
    fun provideSAlarmManagerHelper(@ApplicationContext context: Context): AlarmManagerHelper {
        return AlarmManagerHelper(context)
    }


}