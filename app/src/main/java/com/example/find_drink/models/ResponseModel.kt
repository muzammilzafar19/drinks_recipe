package com.example.find_drink.models

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ResponseModel {
    @SerializedName("drinks")
    @Expose
    var drinks: List<Drinks>? = null
}