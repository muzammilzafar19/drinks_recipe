package com.example.find_drink.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class FavouriteEntity {
    @PrimaryKey()
    var drinkId : Int =0
    var drinkName : String? =""
    var date : String? =""
    var alcholic : String? =""
    var thumnail : String? =""
}