package com.example.find_drink.repository

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.example.find_drink.models.Drinks
import com.example.find_drink.network_module.ApiInterface
import com.example.find_drink.room.dao.FarvouriteDao
import com.example.find_drink.room.entities.FavouriteEntity
import com.example.find_drink.util.ConnectionDetector
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainRepo @Inject constructor(
    val apiInterface: ApiInterface,
    val farvouriteDao: FarvouriteDao,
    val connectionDetector: ConnectionDetector
) {

    fun getAllDrinksByALpha(name: String): LiveData<List<Drinks>> = liveData(Dispatchers.IO)
    {
        try {
            if(!connectionDetector.isNotConnectedToInternet()) {
                val respose = apiInterface.getDrinksByAlphabet(name)
                emit(respose.drinks!!)
            }
            else{
                emit(emptyList())
            }
        } catch (e: Exception) {
            emit(emptyList())
        }
        // emit(respose.)

    }

    fun getAllDrinksByName(name: String): LiveData<List<Drinks>> = liveData(Dispatchers.IO)
    {
        try {
            if(!connectionDetector.isNotConnectedToInternet()) {
                val respose = apiInterface.getDrinksByName(name)
                emit(respose.drinks!!)
            }
            else{
                emit(emptyList())
            }
        } catch (e: Exception) {
            emit(emptyList())
        }
    }

    fun addFavourite(favouriteEntity: FavouriteEntity) {
        CoroutineScope(Dispatchers.IO).launch {
            farvouriteDao.insertFavourite(favouriteEntity)
        }
    }

    fun removeFavourite(drinkid: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            farvouriteDao.removeFavourite(drinkid)
        }
    }

    fun getFavourite(): LiveData<List<FavouriteEntity>> = liveData(Dispatchers.IO)
    {
        emit(farvouriteDao.getSpecificFavourite())
    }

  suspend  fun getSpecificFavourite(drinksid: Int): Int {

        return farvouriteDao.getSpecificFavourite(drinksid)
    }

}