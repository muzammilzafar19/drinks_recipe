package com.example.find_drink.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.find_drink.R
import com.example.find_drink.databinding.FragmentHomeBinding
import com.example.find_drink.ui.adapter.DrinksAdapter
import com.example.find_drink.util.Inflate
import com.example.find_drink.util.SharedPrefs
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {
    val viewModel: HomeViewModel by viewModels()
    val binding: FragmentHomeBinding by Inflate(R.layout.fragment_home)
    var type = 1 // 1: For By Name

    // 2: For By Alphabet
    lateinit var adapter: DrinksAdapter

    @Inject
    lateinit var sharedPrefs: SharedPrefs
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateAdapter(sharedPrefs.getLastSearch()!!)
        binding.searchView.setQuery(sharedPrefs.getLastSearch()!!, true)
        if(sharedPrefs.gettype()==1) {
            binding.byname.isChecked = true
        }
        else{
            binding.byalphabet.isChecked = true

        }
        radioListner()

        searchViewListner()

        viewModel.setRemoveAddCallback {
            if (it) {
                Toast.makeText(requireContext(), "Added in Favourite record", Toast.LENGTH_LONG)
                    .show()
            } else {
                Toast.makeText(requireContext(), "removed from Favourite record", Toast.LENGTH_LONG)
                    .show()

            }
        }

    }

    private fun searchViewListner() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isNotEmpty()) {
                    populateAdapter(query)
                    sharedPrefs.setLastSearch(query)
                    sharedPrefs.settype(type)
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {

                return false
            }
        })
    }

    private fun populateAdapter(name: String) {
        if (type == 1) {
            viewModel.getAllDrinkByNameLD(name).observe(viewLifecycleOwner) { list ->
                binding.progress.visibility = View.GONE

                viewModel.getAllFavouriteLD().observe(viewLifecycleOwner) { favlist ->
                    adapter = DrinksAdapter(list, favlist) {
                        toggleSelection(it)
                        viewModel.addremoveFavourite(list[it])

                    }

                    binding.drinklist.adapter = adapter
                }

                // Log.i("list", Gson().toJson(it))
            }
        } else {
            viewModel.getAllDrinkByAlPhabetLD(name).observe(viewLifecycleOwner) { list ->
                binding.progress.visibility = View.GONE
                viewModel.getAllFavouriteLD().observe(viewLifecycleOwner) { favlist ->
                    adapter = DrinksAdapter(list, favlist) {
                        toggleSelection(it)
                        viewModel.addremoveFavourite(list[it])

                    }

                    binding.drinklist.adapter = adapter
                }
                //Log.i("list", Gson().toJson(list))
            }
        }
    }

    private fun radioListner() {
        binding.radiogroup.setOnCheckedChangeListener { radioGroup, optionId ->
            run {
                when (optionId) {
                    R.id.byname -> {
                        type = 1
                        // do something when radio button 1 is selected
                    }
                    R.id.byalphabet -> {
                        type = 2

                        // do something when radio button 2 is selected
                    }
                }
            }
        }
    }

    private fun toggleSelection(position: Int) {
        try {
            adapter.toggleSelection(position)
            val count: Int = adapter.getSelectedItemCount()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}