package com.example.find_drink.ui.favourite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.find_drink.R
import com.example.find_drink.databinding.FragmentFavouriteBinding
import com.example.find_drink.ui.adapter.FavouriteAdapter
import com.example.find_drink.util.inflate
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavouriteFragment : Fragment() {

    val binding : FragmentFavouriteBinding by inflate(R.layout.fragment_favourite)
    val viewModel : FavouriteViewModel by viewModels()
    lateinit var adapter : FavouriteAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllFavouriteLD().observe(viewLifecycleOwner){favlist->

            adapter=FavouriteAdapter(favlist){
                viewModel.removeFavourite(favlist[it].drinkId)
               // favlist[it]
            }
            binding.favouritelist.adapter=adapter
        }
    }



}