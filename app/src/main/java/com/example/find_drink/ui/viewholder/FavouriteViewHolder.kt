package com.example.find_drink.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.find_drink.databinding.FavouriteListViewBinding
import com.example.find_drink.room.entities.FavouriteEntity

class FavouriteViewHolder(val binding: FavouriteListViewBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind( favouriteEntity : FavouriteEntity)
    {
        binding.apply {
            Glide.with(binding.root.context).load(favouriteEntity.thumnail).circleCrop().into(thumb)
            title.text=favouriteEntity.drinkName
            alcholic.isChecked = favouriteEntity.alcholic=="Alcoholic"
        }
    }
}