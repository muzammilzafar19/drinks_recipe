package com.example.find_drink.ui.favourite

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.find_drink.repository.MainRepo
import com.example.find_drink.room.entities.FavouriteEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
@HiltViewModel
class FavouriteViewModel @Inject constructor (val mainRepo: MainRepo) : ViewModel() {
    fun getAllFavouriteLD() : LiveData<List<FavouriteEntity>> = mainRepo.getFavourite()
    fun removeFavourite(drinkid : Int) = mainRepo.removeFavourite(drinkid)



}